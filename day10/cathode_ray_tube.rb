# frozen_string_literal: true

# Advent of code - Day 10
class CathodeRayTube
  attr_accessor :data, :x, :cycle, :pixels

  def initialize(filename) = @data = File.readlines(filename).map { _1.strip.split }

  def solution(advanced: false) = advanced ? crt_render : strengths_sample

  ## part 1

  def strengths_sample
    @cycle = 1
    @x = 1
    strength = 0

    run { strength += signal_strength if interesting_cycle? }

    strength
  end

  def interesting_cycle? = (@cycle - 20) % 40 == 0

  ## part 2

  def crt_render
    @cycle = 1
    @x = 1
    @pixels = []

    run { @pixels << pixel }

    @pixels.each_slice(40).to_a.map(&:join)
  end

  def pixel = ((h_position - 1)..(h_position + 1)).include?(@x) ? '#' : ' '
  def h_position = @pixels.size % 40
  # @cycle % 40 was off-by-1 error

  ## common

  def signal_strength = cycle * x

  def run(&block)
    changes = []
    loop do
      read_instruction(changes)
      block.call if block_given?
      @cycle += 1
      @x += changes.shift
      break if data.empty?
    end
  end

  def read_instruction(changes)
    return unless changes.empty?

    action, value = data.shift
    case action
    when 'noop' then changes << 0
    when 'addx' then changes.push(0, value.to_i)
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = CathodeRayTube.new(ARGV[0] || 'test.txt')
  puts obj.solution
  obj = CathodeRayTube.new(ARGV[0] || 'test.txt')
  puts obj.solution(advanced: true)
end
