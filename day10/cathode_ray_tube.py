import sys


class CathodeRayTube:
    """ Advent of code - Day 10 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = [line.strip().split(' ') for line in f.readlines()]
        self.strengths = {}
        self.reg_x = 1

    def solution(self, advanced=False):
        return self.crt_render() if advanced else self.signal_strength()

    def signal_strength(self):
        self.strengths = {}

        def strengths_action(cycle):
            if self.interesting_cycle(cycle):
                self.strengths[cycle] = cycle * self.reg_x

        self.run_cycles(strengths_action)

        return sum(self.strengths.values())

    def crt_render(self):
        self.pixels = []
        self.run_cycles(lambda cycle: self.pixels.append(self.pixel(cycle)))
        return self.pixel_rows(self.pixels)

    def run_cycles(self, block):
        for cycle, change in enumerate(self.ops_to_cycles(), start=1):
            block(cycle)
            self.reg_x += change

    def pixel(self, cycle):
        h_position = (cycle - 1) % 40
        if self.reg_x in range(h_position - 1, h_position + 2):
            return '#'
        else:
            return '.'

    @staticmethod
    def pixel_rows(pixels):
        rows = []
        for y in range(0, len(pixels), 40):
            rows.append(''.join(pixels[y:y + 40]))
        return '\n'.join(rows)

    @staticmethod
    def interesting_cycle(num):
        return (num - 20) % 40 == 0

    def ops_to_cycles(self):
        changes = []
        for line in self.data:
            if line[0] == 'noop':
                changes.append(0)
            else:
                changes.extend([0, int(line[1])])
        return changes


if __name__ == "__main__":
    obj = CathodeRayTube(sys.argv[1])
    print(obj.solution(False))
    obj = CathodeRayTube(sys.argv[1])
    print(obj.solution(advanced=True))
