import sys
import os
from dataclasses import dataclass
from termcolor import colored


@dataclass
class Point:
    x: int
    y: int
    value: int
    distance: int = sys.maxsize
    previous = None
    next = None

    def __hash__(self):
        return hash((self.x, self.y))


class HillClimbing:
    """ Advent of code - Day 12 """

    def __init__(self, filename):
        with open(filename) as f:
            self.grid = [list(line.strip()) for line in f.readlines()]
        self.w = len(self.grid[0])
        self.h = len(self.grid)
        self.points = {}
        self.short_path = []
        self.source = None
        self.target = None

    def solution(self, advanced=False):
        return self.shortest_from_a() if advanced else self.shortest_from_s()

    def point(self, x, y):
        if y < 0 or y == self.h or x < 0 or x == self.w:
            return None

        if not self.points.get((x, y)):
            self.points[(x, y)] = Point(x, y, self.value(self.grid[y][x]))
            if self.grid[y][x] == 'S':
                self.source = self.points[(x, y)]
            elif self.grid[y][x] == 'E':
                self.target = self.points[(x, y)]
        return self.points[(x, y)]

    @staticmethod
    def value(letter):
        return ord({'S': 'a', 'E': 'z'}.get(letter, letter)) - ord('a')

    # climbing steepness limit implemented here
    def neighbours(self, x, y, direction='up'):
        around = [
            self.point(x, y - 1),  # North
            self.point(x + 1, y),  # East
            self.point(x, y + 1),  # South
            self.point(x - 1, y),  # West
        ]
        found = list(filter(None, around))  # remove points out of bounds
        if direction == 'up':
            # return only neighbours at most 1 height above current
            return [f for f in found if f.value <= self.point(x, y).value + 1]
        else:
            return [f for f in found if f.value >= self.point(x, y).value - 1]

    def neighbours_down(self, x, y):
        return self.neighbours(x, y, 'down')

    def shortest_from_s(self):
        nodes = self.build_points()
        target = self.dijkstra(self.source,
                               self.neighbours,
                               self.is_target,
                               nodes)
        x = target
        while x:
            self.short_path.insert(0, x)
            x = x.previous
        self.display()
        return target.distance

    # for part 2 we start on E and search shortest path to *any* 'a'
    def shortest_from_a(self):
        nodes = self.build_points()
        target = self.dijkstra(self.target,
                               self.neighbours_down,
                               self.is_low,  # is it any 'a'
                               nodes)
        x = target
        while x:
            self.short_path.insert(0, x)
            x = x.previous
        self.display()
        return target.distance

    def dijkstra(self, source, neighbours, is_target, nodes):
        q = nodes
        source.distance = 0

        # based on https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
        # but with constant steps and return distance, not path
        while len(q) > 0:
            # print(f'{[self.grid[p.y][p.x] for p in q]}')
            u = sorted(q, key=lambda p: p.distance)[0]
            q.remove(u)
            all_neighbours = neighbours(u.x, u.y)
            neighbours_in_q = list(set(q).intersection(set(all_neighbours)))
            for v in neighbours_in_q:
                alt = u.distance + 1  # counting steps, not elevation
                if alt < v.distance:
                    v.distance = alt
                    v.previous = u
                    u.next = v
            if is_target(u.x, u.y):
                target = u
                break
        return target

    def is_target(self, x, y):
        return self.grid[y][x] == 'E'

    def is_source(self, x, y):
        return self.grid[y][x] == 'S'

    def is_low(self, x, y):
        return self.grid[y][x] == 'a' or self.grid[y][x] == 'S'

    def build_points(self):
        coords = [(x, y) for x in range(0, self.w) for y in range(0, self.h)]
        return [self.point(x, y) for (x, y) in coords]

    def display(self):
        if not os.environ.get('DEBUG'):
            return
        print('-' * 80)
        for y in range(0, self.h):
            letters = [self.display_point(self.point(x, y)) for x in range(0, self.w)]
            print(''.join(letters))
        print('-' * 80)

    def display_point(self, point):
        if self.is_source(point.x, point.y):
            return 'S'
        elif self.is_target(point.x, point.y):
            return 'E'
        elif point in self.short_path:
            return colored(self.grid[point.y][point.x], 'green')
        elif point.next is None:
            return colored(self.grid[point.y][point.x], 'red')
        else:
            return self.grid[point.y][point.x]


if __name__ == "__main__":
    obj = HillClimbing(sys.argv[1])
    print(obj.solution(False))
    obj = HillClimbing(sys.argv[1])
    print(obj.solution(advanced=True))
