import unittest
import tempfile
from hill_climbing import HillClimbing


class HillClimbingTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            'Sabqponm',
            'abcryxxl',
            'accszExk',
            'acctuvwj',
            'abdefghi',
        ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return HillClimbing(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 31)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 29)


if __name__ == '__main__':
    unittest.main()
