# Advent of Code 2022

Advent of Code is an Advent calendar of small programming puzzles
for a variety of skill sets and skill levels that can be solved in
any programming language you like. People use them as a speed
contest, interview prep, company training, university coursework,
practice problems, or to challenge each other.

If you want to know more check the [web](https://adventofcode.com/2022/)

