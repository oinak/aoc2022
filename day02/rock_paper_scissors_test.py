import unittest
import tempfile
from rock_paper_scissors import RockPaperScissors


class RockPaperScissorsTest(unittest.TestCase):
    def lines(self):
        return '\n'.join(["A Y", "B X", "C Z"])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return RockPaperScissors(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 15)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 12)

# $ python calorie_counting_test.py
# ..
# ----------------------------------------------------------------------
# Ran 2 tests in 0.001s

# OK


if __name__ == '__main__':
    unittest.main()
