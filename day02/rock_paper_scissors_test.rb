# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './rock_paper_scissors'

describe RockPaperScissors do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { RockPaperScissors.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      A Y
      B X
      C Z
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(15)
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(12)
    end
  end
end
