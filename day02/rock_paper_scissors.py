import os
import sys


class RockPaperScissors:
    """ Advent of code - Day 2 """

    def __init__(self, filename):
        with open(filename) as f:
            self.moves = list(map(lambda x: x.strip(), f.readlines()))

    def solution(self, advanced=False):
        return self.score_guessing() if advanced else self.score()

    def score(self):
        return sum([self.score_for(*line.split()) for line in self.moves])

    CHOICE_SCORE = {'rock': 1, 'paper': 2, 'scissors': 3}
    RESULT_SCORE = {
        'rock':     {'rock': 3, 'paper': 6, 'scissors': 0},
        'paper':    {'rock': 0, 'paper': 3, 'scissors': 6},
        'scissors': {'rock': 6, 'paper': 0, 'scissors': 3},
    }
    YOU_CHOICE = {'X': 'rock', 'Y': 'paper', 'Z': 'scissors'}
    FOE_CHOICE = {'A': 'rock', 'B': 'paper', 'C': 'scissors'}

    def score_for(self, foe, you):
        you_choice = self.YOU_CHOICE[you]
        foe_choice = self.FOE_CHOICE[foe]
        score = self.CHOICE_SCORE[you_choice] \
            + self.RESULT_SCORE[foe_choice][you_choice]
        return score

    # part 2

    RESULT = {'X': 'lose', 'Y': 'tie', 'Z': 'win'}
    CHOICE_GUESS = {
        'rock':     {'win': 'paper',    'tie': 'rock',     'lose': 'scissors'},
        'paper':    {'win': 'scissors', 'tie': 'paper',    'lose': 'rock'},
        'scissors': {'win': 'rock',     'tie': 'scissors', 'lose': 'paper'},
    }

    def score_guessing(self):
        score = 0
        for line in self.moves:
            foe, result = line.strip().split(' ')
            foe_choice = self.FOE_CHOICE[foe]
            you_choice = self.CHOICE_GUESS[foe_choice][self.RESULT[result]]
            score += self.CHOICE_SCORE[you_choice] \
                + self.RESULT_SCORE[foe_choice][you_choice]
            self.debug(foe, foe_choice, result, you_choice)
        return score

    def debug(self, foe, foe_choice, result, you_choice):
        if os.environ.get('DEBUG'):
            print('foe: {} = {}, result: {} = {}, you: {}'.format(
                foe, foe_choice, result, self.RESULT[result], you_choice
            ))
            print(' -> shape: {}, result: {}'.format(
                self.CHOICE_SCORE[you_choice],
                self.RESULT_SCORE[foe_choice][you_choice]
            ))


if __name__ == "__main__":
    obj = RockPaperScissors(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
