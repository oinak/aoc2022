# frozen_string_literal: true

# Advent of code - Day 2
class RockPaperScissors
  attr_reader :data

  def initialize(filename)
    @data = File.readlines(filename).map { |l| l.strip.split }
  end

  def solution(advanced: false)
    advanced ? score_guessing : score
  end

  private

  SHAPE = {
    'A' => :rock,
    'B' => :paper,
    'C' => :scissors,
    'X' => :rock,
    'Y' => :paper,
    'Z' => :scissors
  }.freeze
  SCORE = { loss: 0, tie: 3, win: 6, rock: 1, paper: 2, scissors: 3 }.freeze
  WINS = { rock: :scissors, paper: :rock, scissors: :paper }.freeze
  PLAN = { 'X' => :loss, 'Y' => :tie, 'Z' => :win }.freeze
  LOSS = { rock: :paper, paper: :scissors, scissors: :rock }.freeze

  def score
    data.inject(0) do |score, (foe, you)|
      print_debug(score, foe, you)
      score + SCORE[outcome(SHAPE[foe], SHAPE[you])] + SCORE[SHAPE[you]]
    end
  end

  def outcome(foe, you)
    return :tie if you == foe
    return :win if WINS[you] == foe

    :loss
  end

  def print_debug(score, foe, you)
    return unless ENV['DEBUG']

    puts("   #{format('%8s', foe)} " \
         "vs #{format('%8s', you)}(#{SCORE[you]}) " \
         "=> #{format('%4s', outcome(foe, you))}(#{SCORE[outcome(foe, you)]}) + #{score}")
  end

  # part 2

  def score_guessing
    data.inject(0) do |score, (foe, plan)|
      move = choose_move(foe, PLAN[plan])
      print_debug(score, SHAPE[foe], move)
      score + SCORE[outcome(SHAPE[foe], move)] + SCORE[move]
    end
  end

  def choose_move(foe, plan)
    return SHAPE[foe] if plan == :tie
    return WINS[SHAPE[foe]] if plan == :loss

    LOSS[SHAPE[foe]]
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = RockPaperScissors.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
