import sys


class CalorieCounting:
    """ Advent of code - Day 1 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = f.read()
        self.__sorted_list = []

    def solution(self, advanced=False):
        return self.top_three() if advanced else self.largest_calorie_count()

    def sorted_list(self):
        if self.__sorted_list:
            return self.__sorted_list

        groups = self.data.split('\n\n')
        line_groups = [g.split('\n') for g in groups]

        def make_ints(line_group):
            return [int(line or '0') for line in line_group]  # protect vs ''
        num_groups = list(map(make_ints, line_groups))
        self.__sorted_list = [sum(ng) for ng in num_groups]
        self.__sorted_list.sort(reverse=True)
        return self.__sorted_list

    def largest_calorie_count(self):
        return self.sorted_list()[0]

    def top_three(self):
        return sum(self.sorted_list()[0:3])


if __name__ == "__main__":
    obj = CalorieCounting(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
