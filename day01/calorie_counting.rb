# frozen_string_literal: true

# Advent of code - Day 1
class CalorieCounting
  attr_reader :data

  def initialize(filename) = @data = File.read(filename)

  def solution(advanced: false) = advanced ? top_three : largest_calorie_count

  private

  def sorted_list
    return @sorted_list if defined? @sorted_list

    groups = data.split("\n\n")
    @sorted_list = groups.map { |g| g.split("\n").map(&:to_i).sum }.sort.reverse
  end

  def largest_calorie_count = sorted_list.first

  def top_three = sorted_list.take(3).sum
end

if __FILE__ == $PROGRAM_NAME
  obj = CalorieCounting.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
