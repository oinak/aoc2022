# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './calorie_counting'

describe CalorieCounting do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { CalorieCounting.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      1000
      2000
      3000

      4000

      5000
      6000

      7000
      8000
      9000

      10000
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(24_000)
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(45_000)
    end
  end
end

# $ ruby calorie_counting_test.rb
# Run options: --seed 19961

# # Running:

# ..

# Fabulous run in 0.007551s, 264.8639 runs/s, 264.8639 assertions/s.

# 2 runs, 2 assertions, 0 failures, 0 errors, 0 skips
