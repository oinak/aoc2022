import unittest
import tempfile
from calorie_counting import CalorieCounting


class CalorieCountingTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            "1000", "2000", "3000", "",
            "4000", "",
            "5000", "6000", "",
            "7000", "8000", "9000", "",
            "10000",
            ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return CalorieCounting(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 24000)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 45000)

# $ python calorie_counting_test.py
# ..
# ----------------------------------------------------------------------
# Ran 2 tests in 0.001s

# OK


if __name__ == '__main__':
    unittest.main()
