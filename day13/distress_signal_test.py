import unittest
import tempfile
from distress_signal import DistressSignal


class DistressSignalTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            '[1,1,3,1,1]', '[1,1,5,1,1]', '',
            '[[1],[2,3,4]]', '[[1],4]', '',
            '[9]', '[[8,7,6]]', '',
            '[[4,4],4,4]', '[[4,4],4,4,4]', '',
            '[7,7,7,7]', '[7,7,7]', '',
            '[]', '[3]', '',
            '[[[]]]', '[[]]', '',
            '[1,[2,[3,[4,[5,6,7]]]],8,9]', '[1,[2,[3,[4,[5,6,0]]]],8,9]',
            ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return DistressSignal(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 13)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 140)


if __name__ == '__main__':
    unittest.main()
