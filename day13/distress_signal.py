import sys
import os
from itertools import zip_longest, chain
from functools import cmp_to_key


class DistressSignal:
    """ Advent of code - Day 13 """

    def __init__(self, filename):
        with open(filename) as f:
            text = f.read().strip()
            pairs = [lines.split('\n') for lines in text.split('\n\n')]
            self.data = [(eval(left), eval(right)) for left, right in pairs]

    def solution(self, advanced=False):
        return self.decoder_key() if advanced else self.ordered_pairs()

    def ordered_pairs(self):
        sum = 0
        for ind, (left, right) in enumerate(self.data, start=1):
            self.dprint(f'\n== Pair {ind} ==')
            if self.ordered(left, right) == 1:
                sum += ind
        return sum

    def ordered(self, left, right, d=0):
        self.dprint(f'{"  " * d}- Compare {left} vs {right}')
        if type(left) is int:
            if type(right) is int:
                return self.diff(right, left)
            self.dprint(f'{"  " * d}- Mixed types convert left to {[left]}')
            return self.ordered([left], right, d + 1)
        elif type(right) is int:
            self.dprint(f'{"  " * d}- Mixed types convert right to {[right]}')
            return self.ordered(left, [right], d + 1)
        else:  # both are lists
            return self.list_compare(left, right, d)

    def list_compare(self, left_list, right_list, d=0):
        for left, right in zip_longest(left_list, right_list):
            if left is not None:
                if right is not None:  # 0 is "false"
                    cmp = self.ordered(left, right, d + 1)
                    if cmp == 0:
                        next  # tie, keep looking
                    elif cmp == 1:
                        self.dprint(f'{"  "*(d+1)}- Left side is smaller, so inputs are in the right order')
                        return cmp
                    else:  # cmp = -1
                        self.dprint(f'{"  "*(d+1)}- Right side is smaller, so inputs are NOT in the right order')
                        return cmp
                else:  # right runs out earlier: wrong order
                    self.dprint(f'{"  "*(d+1)}- Right side ran out of items, so inputs are NOT in the right order')
                    return -1
            elif right is not None:  # left runs out earlier: right order
                self.dprint(f'{"  "*(d+1)}- Left side ran out of items, so inputs are in the right order')
                return 1
        return 0  # no break of the for means tie

    def dprint(self, str):
        if os.environ.get('DEBUG'):
            print(str)

    @staticmethod
    def diff(a, b):
        """
        return (a > b) - (a < b)  # ruby/perl's <=> operator
        but explicit > implicit
        """
        if a > b:
            return 1
        elif a < b:
            return -1
        else:
            return 0

    # part 2
    def decoder_key(self):
        packets = [[[2]], [[6]]]
        packets += list(chain(*self.data))
        sorted_packets = sorted(packets, key=cmp_to_key(self.ordered), reverse=True)
        ind_2 = sorted_packets.index([[2]]) + 1
        ind_6 = sorted_packets.index([[6]]) + 1
        return ind_2 * ind_6


if __name__ == "__main__":
    obj = DistressSignal(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
