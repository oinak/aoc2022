# import os
import sys
import re


class NoSpace:
    """ Advent of code - Day 7 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = f.readlines()
        self.tree = {'': {}}
        self.path = ['/']
        self.size = {}
        self.read_filesystem()
        self.sizes(self.tree[''], [''])

    def solution(self, advanced=False):
        return self.advanced() if advanced else self.basic()

    def basic(self):
        small = {d: s for d, s in self.size.items() if s < 100_000}
        return sum([size for _, size in small.items()])

    def advanced(self):
        total = 70_000_000
        needed = 30_000_000
        used = self.size['']
        available = total - used
        required = needed - available
        large_enough = [s for s in sorted(self.size.values()) if s > required]
        return min(large_enough)

    def read_filesystem(self):
        for line in self.data:
            self.read_node(line.strip())

    def read_node(self, line):
        if line == '$ ls':
            pass
        elif line == '$ cd ..':
            self.path.pop()
        elif line == '$ cd /':
            self.path = ['']
        elif dir := re.findall(r'dir (\w+)', line):
            self.dig(self.tree, dir[0], {}, *self.path)
        elif cd := re.findall(r'\$ cd (\w+)', line):
            dirname = cd[0]
            self.dig(self.tree, dirname, {}, *self.path)
            self.path.append(cd[0])
        elif file := re.findall(r'(\d+) ([\w\.]+)', line):
            size, name = file[0]
            self.dig(self.tree, name, int(size), *self.path)

    def sizes(self, node, path):
        if isinstance(node, int):  # file leaf
            return node  # paretn dir will sum and store
        elif isinstance(node, dict):
            nodes = [self.sizes(tree, path+[f]) for f, tree in node.items()]
            size = sum(nodes)
            self.size['/'.join(path)] = size
            return size  # for recursion

    def dig(self, dic, newkey, value, *keys):
        try:
            for key in keys:
                dic = dic[key]
            dic[newkey] = value
            return dic
        except KeyError:
            return None

    # def debug(self, msg):
    #     if os.environ.get('DEBUG'):
    #         print(msg)


if __name__ == "__main__":
    obj = NoSpace(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
