# frozen_string_literal: true

# Advent of code - Day 7
class NoSpace
  attr_accessor :data, :path, :dirs

  def initialize(filename)
    read_dirs(filename)
    print_tree(dirs)
  end

  def solution(advanced: false) = advanced ? smallest_to_delete : total_size_small_dirs

  def total_size_small_dirs = small_dirs(dirs, size_limit: 100_000).values.sum

  TOTAL_SPACE = 70_000_000
  UPDATE_SIZE = 30_000_000

  def smallest_to_delete
    used = size_tree(dirs)
    unused = TOTAL_SPACE - used
    needed = UPDATE_SIZE - unused

    candidates = small_dirs(size_limit: TOTAL_SPACE)
    large_enough = candidates.select { |_path, size| size >= needed }
    large_enough.values.min
  end

  def read_dirs(filename)
    @data = File.readlines(filename).map(&:strip)
    # https://www.programmersought.com/article/488310341585/
    @dirs = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
    @path = ['/']

    data.each { |line| line[0] == '$' ? command_line(line) : entry_line(line) }
  end

  # $ ls : prints out all of the files and directories (entry_line will process those)
  # $ cd / : switches the current directory to the outermost directory, /.
  # $ cd .. : moves out one level
  # $ cd x : move in one level, into subdirectory 'x'
  def command_line(line)
    case line
    when %r{^\$ cd /} then @path = ['']
    when /^\$ cd \.\./  then @path.pop if @path.size > 1
    when /^\$ cd (\w+)/ then @path.append($1) # rubocop:disable Style/PerlBackrefs
    end
  end

  # 'dir x' entries can be ignored thanks to hash's default_proc
  # '123 name.ext' store's file size on the tree
  def entry_line(line)
    md = /^(\d+) ([\w.]+)/.match(line)
    return unless md

    @dirs.dig(*@path).store(md[2], md[1])
  end

  # recurses the dir tree, returns a hash of path sizes
  def small_dirs(tree = dirs, list = {}, prefix: [], size_limit: 100_000)
    tree.each do |(name, contents)|
      next unless contents.is_a? Hash # omit files

      size = size_tree(contents)
      path = prefix + [name] # dir names are NOT unique, but paths ared
      list[path.join('/')] = size if size <= size_limit
      small_dirs(contents, list, prefix: path, size_limit:)
    end
    list
  end

  def size_tree(node = nil)
    return node.to_i unless node.is_a? Hash

    node.inject(0) { |size, (_, value)| size + size_tree(value) }
  end

  # Display tree in the statement format
  def print_tree(node, depth = 0)
    return unless ENV['DEBUG'] == '1'

    puts "\nTree:" if depth == 0

    node.each do |k, value|
      if value.is_a?(Hash)
        puts "#{'  ' * depth}- #{k} (dir)"
        print_tree(value, depth + 1)
      else
        puts "#{'  ' * depth}- #{k} (file, size=#{value})"
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = NoSpace.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
