import unittest
import tempfile
from no_space import NoSpace


class NoSpaceTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            '$ cd /',
            '$ ls',
            'dir a',
            '14848514 b.txt',
            '8504156 c.dat',
            'dir d',
            '$ cd a',
            '$ ls',
            'dir e',
            '29116 f',
            '2557 g',
            '62596 h.lst',
            '$ cd e',
            '$ ls',
            '584 i',
            '$ cd ..',
            '$ cd ..',
            '$ cd d',
            '$ ls',
            '4060174 j',
            '8033020 d.log',
            '5626152 d.ext',
            '7214296 k',
        ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return NoSpace(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 95437)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 24933642.)


if __name__ == '__main__':
    unittest.main()
