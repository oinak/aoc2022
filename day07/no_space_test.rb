# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './no_space'

describe NoSpace do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { NoSpace.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      $ cd /
      $ ls
      dir a
      14848514 b.txt
      8504156 c.dat
      dir d
      $ cd a
      $ ls
      dir e
      29116 f
      2557 g
      62596 h.lst
      $ cd e
      $ ls
      584 i
      $ cd ..
      $ cd ..
      $ cd d
      $ ls
      4060174 j
      8033020 d.log
      5626152 d.ext
      7214296 k
    TXT
  end

  describe 'read_trees' do
    it 'returns the tree' do
      _(subject.dirs).must_equal(
        {
          '' => {
            'a' => {
              'e' => { 'i' => '584' },
              'f' => '29116',
              'g' => '2557',
              'h.lst' => '62596'
            },
            'b.txt' => '14848514',
            'c.dat' => '8504156',
            'd' => {
              'j' => '4060174',
              'd.log' => '8033020',
              'd.ext' => '5626152',
              'k' => '7214296'
            }
          }
        }
      )
    end
  end

  describe 'size_tree' do
    it 'returns the accumulated tree size' do
      _(subject.size_tree(subject.dirs)).must_equal(48_381_165)
    end
  end

  describe 'small_dirs' do
    it 'returns the dirs with at most 100k' do
      _(subject.small_dirs).must_equal({ '/a' => 94_853, '/a/e' => 584 })
    end
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.total_size_small_dirs).must_equal(95_437)
    end
  end

  describe 'with repeated dir names' do
    let(:lines) do
      <<~TXT
        $ cd /
        $ ls
        dir a
        dir b
        10 A.txt
        $ cd a
        $ ls
        dir b
        $ cd b
        $ ls
        20 B.txt
        $ cd ..
        $ cd ..
        $ cd b
        $ ls
        30 C.txt
      TXT
    end

    describe 'read_trees' do
      it 'returns the tree' do
        _(subject.dirs).must_equal(
          {
            '' => {
              'a' => { 'b' => { 'B.txt' => '20' } },
              'b' => { 'C.txt' => '30' },
              'A.txt' => '10'
            }
          }
        )
      end
    end
    describe 'small_dirs' do
      it 'returns the dirs with at most 100k' do
        _(subject.small_dirs).must_equal(
          { '' => 60, '/a' => 20, '/b' => 30, '/a/b' => 20 }
        )
      end
    end
  end

  describe 'size of smallest required to update' do
    it 'returns the size of d' do
      _(subject.smallest_to_delete).must_equal(24_933_642)
    end
  end

  describe 'with advanced situation' do
    it 'returns smallest_to_delete' do
      _(subject.solution(advanced: true)).must_equal(24_933_642)
    end
  end
end
