# frozen_string_literal: true

# Advent of code - Day 8
class TreeHouse
  SIDES = %w[left right up down].freeze

  attr_reader :rows, :width, :height

  def initialize(filename)
    @rows = File.readlines(filename).map { |l| l.strip.chars.map(&:to_i) }
    @width = @rows[0].size
    @height = @rows.size
  end

  def solution(advanced: false) = advanced ? highest_scenic_score : visible_from_edge
  def visible_from_edge = tiles.count { |(r, c)| visible?(r, c) }
  def tiles = (0..(height - 1)).to_a.product((0..(width - 1)).to_a)
  def cols = @cols ||= rows.transpose
  def value(row, col) = @rows[row][col]
  def visible?(row, col)= edge?(row, col) || SIDES.any? { |side| visible_from?(side, row, col) }
  def edge?(row, col) = row == 0 || col == 0 || row == height - 1 || col == width - 1
  def visible_from?(side, row, col) = send(side, row, col).all? { |v| v < value(row, col) }
  def left(row, col) = rows[row][...col].reverse
  def right(row, col) = rows[row][(col + 1)..]
  def up(row, col) = cols[col][...row].reverse
  def down(row, col) = cols[col][(row + 1)..]

  # Part 2
  def highest_scenic_score = tiles.map { |r, c| scenic_score(r, c) }.max
  def scenic_score(row, col) = SIDES.map { |s| view_distance(row, col, s) }.inject(:*)

  def view_distance(row, col, side)
    send(side, row, col).inject(0) do |count, val|
      # the tricky bit is counting the visible trees and the 1st blocking one
      break count + 1 if val >= value(row, col)

      count + 1 if val <= value(row, col)
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = TreeHouse.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
