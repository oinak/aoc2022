import sys
from functools import reduce


class TreeHouse:
    """ Advent of code - Day 8 """

    def __init__(self, filename):
        with open(filename) as f:
            lines = [line.strip() for line in f.readlines()]
            self.grid = [[int(c) for c in line] for line in lines]
        self.w = len(self.grid[0])
        self.h = len(self.grid)

    def solution(self, advanced=False):
        return self.highest_scenic() if advanced else self.count_visible()

    def count_visible(self):
        return sum(self.visible(x, y) for x, y in self.coords())

    def coords(self):
        return [(x, y) for x in range(0, self.w) for y in range(0, self.h)]

    def visible(self, x, y):
        if self.is_edge(x, y):
            return True
        lines = self.lines_for(x, y)
        tree = self.grid[y][x]
        visible_lines = [all([val < tree for val in line]) for line in lines]
        return any(visible_lines)

    def lines_for(self, x, y):
        """ visibility does not care about lines' trees order,
        but viewing_distance does, it needs to 'look outwards' """
        return [
            list(reversed([self.grid[yn][x] for yn in range(0, y)])),  # North
            [self.grid[y][xn] for xn in range(x+1, self.w)],           # East
            [self.grid[yn][x] for yn in range(y+1, self.h)],           # South
            list(reversed([self.grid[y][xn] for xn in range(0, x)])),  # West
        ]

    def is_edge(self, x, y):
        return x == 0 or y == 0 or x == self.w - 1 or y == self.h - 1

    # part 2

    def highest_scenic(self):
        scores = [self.scenic_score(x, y) for (x, y) in self.coords()]
        return max(scores)

    def scenic_score(self, x, y):
        if self.is_edge(x, y):
            return 0
        lines = self.lines_for(x, y)
        tree = self.grid[y][x]
        distances = [self.view_distance(line, tree) for line in lines]
        return reduce(lambda i, j: i * j, distances)

    def view_distance(self, line, height):
        distance = 0
        for tree in line:
            distance += 1
            if tree >= height:  # can't see beyond
                break
        return distance


if __name__ == "__main__":
    obj = TreeHouse(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
