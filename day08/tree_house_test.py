import unittest
import tempfile
from tree_house import TreeHouse


class TreeHouseTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
           '30373',
           '25512',
           '65332',
           '33549',
           '35390',
        ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return TreeHouse(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 21)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 8)


if __name__ == '__main__':
    unittest.main()
