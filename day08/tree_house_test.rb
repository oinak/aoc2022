# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './tree_house'

describe TreeHouse do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { TreeHouse.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      30373
      25512
      65332
      33549
      35390
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(21)
    end
  end

  describe 'with advanced situation' do
    it 'view_distance' do
      _(subject.view_distance(1, 2, :left)).must_equal(1)
      _(subject.view_distance(1, 2, :up)).must_equal(1)
      _(subject.view_distance(1, 2, :right)).must_equal(2)
      _(subject.view_distance(1, 2, :down)).must_equal(2)
    end

    it 'scenic_score' do
      _(subject.scenic_score(1, 2)).must_equal(4)
      _(subject.scenic_score(3, 2)).must_equal(8)
    end

    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(8)
    end
  end
end
