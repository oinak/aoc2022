import unittest
import tempfile
from monkey_middle import MonkeyMiddle


class MonkeyMiddleTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            'Monkey 0:',
            '  Starting items: 79, 98',
            '  Operation: new = old * 19',
            '  Test: divisible by 23',
            '    If true: throw to monkey 2',
            '    If false: throw to monkey 3',
            '',
            'Monkey 1:',
            '  Starting items: 54, 65, 75, 74',
            '  Operation: new = old + 6',
            '  Test: divisible by 19',
            '    If true: throw to monkey 2',
            '    If false: throw to monkey 0',
            '',
            'Monkey 2:',
            '  Starting items: 79, 60, 97',
            '  Operation: new = old * old',
            '  Test: divisible by 13',
            '    If true: throw to monkey 1',
            '    If false: throw to monkey 3',
            '',
            'Monkey 3:',
            '  Starting items: 74',
            '  Operation: new = old + 3',
            '  Test: divisible by 17',
            '    If true: throw to monkey 0',
            '    If false: throw to monkey 1',
        ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return MonkeyMiddle(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 10605)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 2713310158)


if __name__ == '__main__':
    unittest.main()
