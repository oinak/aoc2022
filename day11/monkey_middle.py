import sys
import re
from functools import reduce


class MonkeyMiddle:
    """ Advent of code - Day 11 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = f.read()

    def solution(self, advanced=False):
        return self.advanced() if advanced else self.basic()

    def basic(self):
        self.relax = True
        self.read()
        [self.round(n) for n in range(1, 21)]
        return Monkey.business()

    def advanced(self):
        self.relax = False
        self.read()
        [self.round(n) for n in range(1, 10001)]
        return Monkey.business()

    def round(self, n):
        for monkey in Monkey.all():
            monkey.turn()
        self.print(f'\nAfter round {n}, the monkeys are holding items with these worry levels:')
        [self.print(m) for m in Monkey.all()]
        self.print('')
        [self.print(f'Monkey {m.id} inspected items {m.inspections} times.') for m in Monkey.all()]
        self.print('')

    def read(self):
        for chunk in self.data.split('\n\n'):
            self.parse_monkey(chunk)

    monkey_regex = re.compile(
        r"Monkey\s(?P<monkey>\d+):\n"
        + r"\s\sStarting\sitems:\s(?P<items>.*)\n"
        + r"\s\sOperation:\snew\s=\sold\s(?P<operator>.)\s(?P<operand>.*)\n"
        + r"\s\sTest:\sdivisible\sby\s(?P<divisor>\d+)\n"
        + r"\s\s\s\sIf\strue:\sthrow to monkey\s(?P<true_to>\d+)\n"
        + r"\s\s\s\sIf\sfalse:\sthrow to monkey\s(?P<false_to>\d+)")

    def parse_monkey(self, text):
        self.print(f're:\n{self.monkey_regex}\n\ntext:\n{text}')
        result = self.monkey_regex.search(text)
        if result:
            return Monkey.create(result.groupdict(), self.relax)
        raise TypeError

    def print(self, *args):
        if self.relax:
            print(*args)


class Monkey:
    __monkeys = {}

    @classmethod
    def create(klass, data, relax):
        monkey = Monkey(data, relax)
        if monkey:
            klass.__monkeys[monkey.id] = monkey
        else:
            raise TypeError

    @classmethod
    def find(klass, id):
        found = klass.__monkeys[id]
        if found:
            return found
        else:
            raise KeyError

    @classmethod
    def business(klass):
        biz = sorted(klass.__monkeys.values(), key=lambda x: x.inspections)
        return biz[-1].inspections * biz[-2].inspections

    @classmethod
    def all(klass):
        return iter(sorted(klass.__monkeys.values(), key=lambda x: x.id))

    __common_divisor = None

    @classmethod
    def common_divisor(klass):
        if not klass.__common_divisor:
            divisors = [m.divisor for m in klass.all()]
            klass.__common_divisor = reduce(lambda v, e: v * e, divisors)
        return klass.__common_divisor

    def __init__(self, data, relax):
        self.id = int(data['monkey'])
        self.items = [int(i) for i in data['items'].split(', ')]
        self.operator = data['operator']
        self.operand = data['operand']
        self.divisor = int(data['divisor'])
        self.true_to = int(data['true_to'])
        self.false_to = int(data['false_to'])
        self.inspections = 0
        self.relax = relax

    def __repr__(self):
        items = ", ".join(str(i) for i in self.items)
        return f'Monkey {self.id}: {items}'

    def turn(self):
        self.print(f'Monkey {self.id}:')
        for index in range(0, len(self.items)):
            if self.relax:
                self.process(index)
            else:
                self.process2(index)

    def process(self, index):
        worry, self.items = self.items[0], self.items[1:]
        self.print(f'\n  Monkey inspects an item with a worry level of {worry}')
        self.inspections += 1          # inspect
        worry = self.operation(worry)

        worry = worry // 3             # relax
        self.print(f'    Monkey gets bored with item. Worry level is divided by 3 to {worry}.')

        if worry % self.divisor == 0:  # test & throw
            self.print(f'    Current worry level is divisible by ({self.divisor}).')
            self.throw(worry, self.true_to)
        else:
            self.print(f'    Current worry level is not divisible by ({self.divisor}).')
            self.throw(worry, self.false_to)

    def process2(self, index):
        worry, self.items = self.items[0], self.items[1:]
        self.print(f'\n  Monkey inspects an item with a worry level of {worry}')
        # inspect
        self.inspections += 1
        worry = self.operation(worry)
        # number reduction via common divisor
        worry = worry % Monkey.common_divisor()  # 9699690
        # test
        if worry % self.divisor == 0:
            self.print(f'    Current worry level is divisible by ({self.divisor}).')
            self.throw(worry, self.true_to)
        else:
            self.print(f'    Current worry level is not divisible by ({self.divisor}).')
            self.throw(worry, self.false_to)
        # throw

    def operation(self, item):
        match [self.operator, self.operand]:
            case['+', 'old']:
                self.print(f'    Worry level doubles to {item + item}.')
                return item + item
            case['*', 'old']:
                self.print(f'    Worry level is multiplied by itself to {item * item}.')
                return item * item
            case['+', str(num)]:
                self.print(f'    Worry level increases by {num} to {item + int(num)}.')
                return item + int(num)
            case['*', str(num)]:
                self.print(f'    Worry level is multiplied by {num} to {item * int(num)}.')
                return item * int(num)

    def throw(self, worry, num):
        self.print(f'    Item with worry level {worry} is thrown to monkey {num}')
        Monkey.find(num).catch(worry)

    def catch(self, item):
        self.items.append(item)

    def print(self, *args):
        if self.relax:
            print(*args)


if __name__ == "__main__":
    obj = MonkeyMiddle(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
