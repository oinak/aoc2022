import sys
# import os
from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])


class RopeBridge:
    """ Advent of code - Day 9 """

    def __init__(self, filename):
        with open(filename) as f:
            lines = [line.strip().split(' ') for line in f.readlines()]
        self.moves = [(dir, int(distance)) for (dir, distance) in lines]
        self.visited = {Point(0, 0): 1}  # tail starting point

    def solution(self, advanced=False):
        return self.trail_size(10) if advanced else self.trail_size(2)

    def trail_size(self, length):
        self.create_rope(length)
        self.move_rope()
        return len(self.visited)

    def create_rope(self, length):
        self.rope = []
        for n in range(0, length):
            self.rope.append(Point(0, 0))

    def move_rope(self):
        for (direction, distance) in self.moves:
            for i in range(0, distance):
                self.move(direction)
            # self.state()

    steps = {
        'U': Point(0, 1),  # up
        'R': Point(1, 0), 'L': Point(-1, 0),  # left/right
        'D': Point(0, -1),  # down
    }

    def move(self, dir):
        head = self.head()
        step = self.steps[dir]
        self.rope[0] = Point(head.x + step.x, head.y + step.y)
        [self.follow(index) for index in range(0, len(self.rope) - 1)]
        self.record_tail_visit()

    def head(self):
        return self.rope[0]

    def tail(self):
        return self.rope[-1]

    def follow(self, index):
        lead = self.rope[index]
        next = self.rope[index + 1]
        if next != lead and not self.touching(lead, next):
            self.rope[index + 1] = self.new_pos(lead, next)

    def new_pos(self, lead, next):
        x = next.x + self.diff(lead.x, next.x)
        y = next.y + self.diff(lead.y, next.y)
        return Point(x, y)

    def record_tail_visit(self):
        value = (self.visited.get(self.tail()) or 0) + 1
        self.visited[self.tail()] = value

    @staticmethod
    def touching(lead, next):
        return (abs(lead.x - next.x) < 2) and (abs(lead.y - next.y) < 2)

    @staticmethod
    def diff(a, b):
        """
        return (a > b) - (a < b)  # ruby/perl's <=> operator
        but explicit > implicit
        """
        if a > b:
            return 1
        elif a < b:
            return -1
        else:
            return 0

    # debugging
    # def state(self):
    #     if not os.environ.get('DEBUG'):
    #         return
    #     min_y = min(p.y for p in self.visited.keys())
    #     min_x = min(p.x for p in self.visited.keys())
    #     max_y = max(p.y for p in self.visited.keys())
    #     max_x = max(p.x for p in self.visited.keys())
    #     print('-' * 10)
    #     for y in reversed(range(min_y, max_y + 5)):
    #         for x in range(min_x, max_x + 5):
    #             print(self.token(Point(x, y)), end='')
    #         print('')
    #     print('  ', end='')
    #     print('-' * 10)

    # def token(self, point):
    #     if self.tail() == point:
    #         return 'T'
    #     elif self.head() == point:
    #         return 'H'
    #     elif Point(0, 0) == point:
    #         return 's'
    #     elif point in self.visited.keys():
    #         return '#'
    #     elif point in self.rope:
    #         return self.rope.index(point)
    #     else:
    #         return '.'


if __name__ == "__main__":
    obj = RopeBridge(sys.argv[1])
    print(obj.solution(False))
    obj = RopeBridge(sys.argv[1])
    print(obj.solution(advanced=True))
