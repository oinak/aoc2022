# frozen_string_literal: true

# Advent of code - Day 9
class RopeBridge
  attr_reader :moves

  STEPS = {
    'U' => [0, 1],
    'D' => [0, -1],
    'L' => [-1, 0],
    'R' => [1, 0]
  }.freeze

  Point = Data.define(:x, :y).freeze

  def initialize(filename)
    @moves = File.readlines(filename).map { _1.strip.split }
    @visited = Hash.new(0).merge([0, 0] => 1)
  end

  def solution(advanced: false)
    advanced ? positions_visited_by_tail(10) : positions_visited_by_tail(2)
  end

  def positions_visited_by_tail(rope_size)
    @rope = [[0, 0]] * rope_size
    follow_moves
    @visited.keys.size
  end

  def follow_moves
    moves.each do |(dir, distance)|
      puts "\n== #{dir} #{distance} ==\n" if debug?
      distance.to_i.times { pull_rope(dir) }
    end
  end

  def pull_rope(dir)
    move_head(dir)
    (1...@rope.size).each { |i| move_knot(i) } # move_tail
    puts "\n#{inspect}\n" if debug?
  end

  def move_head(dir) = @rope[0] = [head.x + STEPS[dir][0], head.y + STEPS[dir][1]]

  def move_knot(pos)
    return if knot(pos) == knot(pos - 1) || touching?(knot(pos), knot(pos - 1))

    @rope[pos] = [new_value(pos, :x), new_value(pos, :y)]
    @visited[@rope[pos]] += 1 if pos == (@rope.size - 1)
  end

  def knot(pos) = Point.new(x: @rope[pos][0], y: @rope[pos][1])
  def head = knot(0)
  def touching?(k_a, k_b) = (k_a.x - k_b.x).abs < 2 && (k_a.y - k_b.y).abs < 2
  def new_value(pos, axis) = knot(pos).send(axis) + (axis_diff(pos, axis) <=> 0)
  def axis_diff(pos, axis) = knot(pos - 1).send(axis) - knot(pos).send(axis)

  ## only for debugging visualization:

  def debug? = ENV['DEBUG'] == '1'
  def inspect = max_y.downto(min_y).map { |y| min_x.upto(max_x).map { |x| cell(x, y) }.join }.join("\n")
  def max_y = (@rope + @visited.keys).map(&:last).max + 1
  def max_x = (@rope + @visited.keys).map(&:first).max + 1
  def min_y = (@rope + @visited.keys).map(&:last).min - 1
  def min_x = (@rope + @visited.keys).map(&:first).min - 1

  def cell(pos_x, pos_y)
    case # rubocop:disable Style/EmptyCaseCondition
    when @rope[0] == [pos_x, pos_y] then 'H'
    when @rope.include?([pos_x, pos_y]) then @rope.find_index([pos_x, pos_y])
    when [pos_x, pos_y] == [0, 0] then 's'
    when @visited.key?([pos_x, pos_y]) then '#'
    else
      '.'
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = RopeBridge.new(ARGV[0] || 'test.txt')
  puts obj.solution
  puts obj.solution(advanced: true)
end
