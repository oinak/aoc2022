# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './rope_bridge'

describe RopeBridge do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { RopeBridge.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
      R 4
      U 4
      L 3
      D 1
      R 4
      D 1
      L 5
      R 2
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(13)
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(1)
    end

    describe 'with larger input' do
      let(:lines) do
        <<~TXT
          R 5
          U 8
          L 8
          D 3
          R 17
          D 10
          L 25
          U 20
        TXT
      end

      it 'returns the cost of cheapest aligment' do
        _(subject.solution(advanced: true)).must_equal(36)
      end
    end
  end
end
