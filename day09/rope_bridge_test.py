import unittest
import tempfile
from rope_bridge import RopeBridge


class RopeBridgeTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            'R 4', 'U 4', 'L 3', 'D 1', 'R 4', 'D 1', 'L 5', 'R 2',
        ])

    def test_basic(self):
        tmpfile = tempfile.NamedTemporaryFile()
        with open(tmpfile.name, 'w') as tmp:
            tmp.write(self.lines())
        subject = RopeBridge(tmpfile.name)
        self.assertEqual(subject.solution(), 13)

    def test_advanced(self):
        tmpfile = tempfile.NamedTemporaryFile()
        with open(tmpfile.name, 'w') as tmp:
            tmp.write(self.lines())
        subject = RopeBridge(tmpfile.name)
        self.assertEqual(subject.solution(advanced=True), 1)

    def lines_large(self):
        return '\n'.join([
            'R 5', 'U 8', 'L 8', 'D 3', 'R 17', 'D 10', 'L 25', 'U 20',
        ])

    def test_advanced_large(self):
        tmpfile = tempfile.NamedTemporaryFile()
        with open(tmpfile.name, 'w') as tmp:
            tmp.write(self.lines_large())
        subject = RopeBridge(tmpfile.name)
        self.assertEqual(subject.solution(advanced=True), 36)


if __name__ == '__main__':
    unittest.main()
