# frozen_string_literal: true

# Advent of code - Day 3
class RucksackReorganization
  attr_reader :data

  def initialize(filename)
    @data = File.readlines(filename).map(&:strip)
  end

  def solution(advanced: false)
    advanced ? badges : repeated_items
  end

  def repeated_items
    data.inject(0) { |sum, line| sum + priority(repeated_item(line)) }
  end

  def repeated_item(line)
    middle = line.length / 2
    left = line[...middle]
    right = line[middle...]
    (left.chars & right.chars)[0]
  end

  LOWER_OFFSET = 'a'.ord - 1
  UPPER_OFFSET = 'A'.ord - 27

  def priority(char)
    case char
    when /[a-z]/ then char.ord - LOWER_OFFSET
    when /[A-Z]/ then char.ord - UPPER_OFFSET
    else
      raise "unsupported char '#{char}'"
    end
  end

  # part 2
  def badges
    @data.each_slice(3).inject(0) do |sum, group|
      sum + priority(common_item(*group))
    end
  end

  def common_item(line1, line2, line3)
    (line1.chars & line2.chars & line3.chars)[0]
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = RucksackReorganization.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
