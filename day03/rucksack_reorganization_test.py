import unittest
import tempfile
from rucksack_reorganization import RucksackReorganization


class RucksackReorganizationTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            'vJrwpWtwJgWrhcsFMMfFFhFp',
            'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
            'PmmdzqPrVvPwwTWBwg',
            'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
            'ttgJtRGJQctTZtZT',
            'CrZsJsPPZsGzwwsLwLmpwMDw',
        ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return RucksackReorganization(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 157)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 70)


if __name__ == '__main__':
    unittest.main()
