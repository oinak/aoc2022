import sys


class RucksackReorganization:
    """ Advent of code - Day 3 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = list(map(lambda x: x.strip(), f.readlines()))

    def solution(self, advanced=False):
        return self.advanced() if advanced else self.basic()

    def basic(self):
        return sum(self.priority(self.repeated(line)) for line in self.data)

    def repeated(self, line):
        half = int(len(line) / 2)
        left, right = line[:half], line[half:]
        return self.intersection([left, right])[0]

    # def intersection(self, list1, list2):
    #     return [value for value in list1 if value in list2]

    def priority(self, letter):
        if letter.islower():
            return ord(letter) - ord('a') + 1
        else:
            return ord(letter) - ord('A') + 27

    # part 2
    def advanced(self):
        sum = 0
        for i in range(0, len(self.data), 3):
            elfs = self.data[i:i+3]
            badge = self.intersection(elfs)
            sum += self.priority(badge)
        return sum

    def intersection(self, lists):
        return list(set.intersection(*map(set, lists)))[0]


if __name__ == "__main__":
    obj = RucksackReorganization(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
