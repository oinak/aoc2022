#!/usr/bin/env python

from datetime import date
import os
import re
import sys


class InitDay:
    """ Day scaffold generator"""

    def __init__(self, title, day=''):
        self.day = int(day or date.today().day)
        self.title = title
        self.__path = ''

    def generate(self):
        self.create_dir()
        self.create_files()

    def create_files(self):
        prefix = self.path() + "/" + self.underscore(self.title)
        self.create_file(prefix + ".py", self.file_draft())
        self.create_file(prefix + "_test.py", self.test_draft())
        self.create_file(self.path() + "/statement.md", 'FILL ME')
        self.create_file(self.path() + "/input.txt", 'FILL ME')
        self.create_file(self.path() + "/test.txt", 'FILL ME')

    def create_file(self, filepath, content):
        if os.path.exists(filepath):
            return
        with open(filepath, 'w') as f:
            f.write(content)

    def underscore(self, str):
        word = '{:s}'.format(str)  # dup
        word = re.sub('::', '/', word)
        word = re.sub(r'([A-Z]+)([A-Z][a-z])', r'\1_\2', word)
        word = re.sub(r'([a-z\d])([A-Z])', r'\1_\2', word)
        word = re.sub('-', '_', word)
        word = word.lower()
        return word

    def create_dir(self):
        if os.path.exists(self.path()):
            return
        os.mkdir(self.path())

    def path(self):
        if not self.__path:
            self.__path = './day{:02d}'.format(self.day)
        return self.__path

    def file_draft(self):
        return """\
import sys


class {title}:
    \"\"\" Advent of code - Day {day} \"\"\"

    def __init__(self, filename):
        with open(filename) as f:
            self.data = f.read()

    def solution(self, advanced=False):
        return 1 if advanced else 0


if __name__ == "__main__":
    obj = {title}(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
""".format(title=self.title, day=self.day)

    def test_draft(self):
        return """\
import unittest
import tempfile
from {file} import {title}


class {title}Test(unittest.TestCase):
    def lines(self):
        return '\\n'.join(['INPUT', 'DATA'])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return {title}(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 15)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 12)


if __name__ == '__main__':
    unittest.main()
""".format(title=self.title, file=self.underscore(self.title))


if __name__ == '__main__':
    day = sys.argv[2] if len(sys.argv) > 2 else ''
    obj = InitDay(title=sys.argv[1], day=day)
    obj.generate()
