import sys


class CampCleanup:
    """ Advent of code - Day 4 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = list(map(lambda x: x.strip(), f.readlines()))

    def solution(self, advanced=False):
        return self.count_overlaps() if advanced else self.contained_pairs()

    def pairs(self):
        return [map(self.pair, line.split(',')) for line in self.data]

    def pair(self, text):
        low, high = text.split('-')
        return range(int(low), int(high) + 1)

    def full_overlap(self, one, other):
        return self.cover(one, other) or self.cover(other, one)

    def cover(self, cover, covered_by):
        return set(cover).intersection(covered_by) == set(covered_by)

    # part 2

    def count_filter(self, filter):
        return len([1 for pair in self.pairs() if filter(*pair)])

    def count_overlaps(self):
        return self.count_filter(self.some_overlap)

    def some_overlap(self, one, other):
        return len(set(one).intersection(other)) > 0

    # part 1 generalised
    def contained_pairs(self):
        return self.count_filter(self.full_overlap)


if __name__ == "__main__":
    obj = CampCleanup(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
