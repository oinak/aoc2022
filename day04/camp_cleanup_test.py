import unittest
import tempfile
from camp_cleanup import CampCleanup


class CampCleanupTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            '2-4,6-8',
            '2-3,4-5',
            '5-7,7-9',
            '2-8,3-7',
            '6-6,4-6',
            '2-6,4-8',
        ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return CampCleanup(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 2)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 4)


if __name__ == '__main__':
    unittest.main()
