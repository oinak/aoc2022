# frozen_string_literal: true

# Advent of code - Day 4
class CampCleanup
  attr_reader :data

  def initialize(filename) = @data = File.readlines(filename).map(&:strip)

  def solution(advanced: false) = advanced ? overlapping_pairs : containing_pairs

  def containing_pairs = pairs.count { |(a, b)| a.cover?(b) || b.cover?(a) }

  def overlapping_pairs = pairs.count { |(a, b)| a.to_a.intersect?(b.to_a) }

  def pairs = @pairs ||= data.map { |line| line.split(',').map { to_range _1 } }

  def to_range(codes)
    limits = codes.split('-').map(&:to_i)
    limits.first..limits.last
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = CampCleanup.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
