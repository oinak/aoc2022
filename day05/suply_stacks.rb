#!/bin/env ruby
# frozen_string_literal: true

# Advent of code - Day 5
class SupplyStacks
  attr_accessor :data, :stacks, :advanced

  def initialize(filename)
    @data = File.readlines(filename)
    @stacks = []
  end

  def solution(advanced: false)
    @advanced = advanced
    top_of_stacks
  end

  def top_of_stacks
    data.each do |line|
      case line
      when /^( 1|$)/ then next
      when /^move/ then read_move_line(line)
      else
        read_stack_line(line)
      end
    end
    stacks.map(&:last).join
  end

  MOVE_REGEX = /move (?<crates>\d+) from (?<from>\d+) to (?<to>\d+)/

  def read_move_line(line)
    md = MOVE_REGEX.match(line)
    raise 'Error parsing move line' unless md

    crates = md[:crates].to_i
    from = md[:from].to_i - 1
    to = md[:to].to_i - 1

    make_move(crates, from, to)
    debug_stacks(line)
  end

  def make_move(crates, from, to)
    moving = stacks[from].pop(crates)
    moving.reverse! unless advanced
    stacks[to].push(*moving)
  end

  def read_stack_line(line)
    line.chars.each_slice(4).with_index do |chars, index|
      stacks[index] ||= []
      stacks[index].prepend(chars[1]) if chars[1].match(/[A-Z]/)
    end
  end

  def debug_stacks(line)
    return unless ENV['DEBUG'] == '1'

    puts "\n#{line}\n"
    stacks.each_with_index { |s, i| puts "#{i + 1} #{s.join}" }
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = SuplyStacks.new(ARGV[0])
  puts obj.solution(advanced: false)
  puts obj.solution(advanced: true)
end
