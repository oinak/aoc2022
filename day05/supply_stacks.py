import sys
import re
import os


class SupplyStacks:
    """ Advent of code - Day 5 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = f.read()
            self.stacks = []
            self.moves = []

    def solution(self, advanced=False):
        return self.top_crates(reverse=False) if advanced else self.top_crates()

    def top_crates(self, reverse=True):
        self.move_crates(reverse)
        return ''.join([s[-1] for s in self.stacks])

    def move_crates(self, reverse):
        self.read_plan()
        for move in self.moves:
            self.run_move(move, reverse)

    def run_move(self, move, reverse):
        stack_from = self.stacks[move['from']-1]
        stack_to = self.stacks[move['to']-1]
        num_crates = move['num']
        crane = stack_from[-num_crates:]
        stack_from[-num_crates:] = []
        if reverse:
            crane.reverse()
        stack_to.extend(crane)
        self.print_stacks(self.stacks)

    def read_plan(self):
        stacks_txt, moves_txt = self.data.split('\n\n')
        self.stacks = self.read_stacks(stacks_txt.split('\n'))
        self.moves = self.read_moves(moves_txt.split('\n'))
        self.print_stacks(self.stacks)

    @staticmethod
    def print_stacks(stacks):
        if not os.environ.get('DEBUG'):
            return
        print('stacks:\n')
        for i, s in enumerate(stacks):
            print('  {}: {}'.format(i + 1, "".join(s)))

    # @staticmethod
    def read_stacks(self, lines):
        lines.pop()
        stacks = [[]]
        for line in lines:
            found = re.findall(r'(\[(\w)\]|\s\s\s\s)', line)
            contents = [f[1] for f in found]
            for i, content in enumerate(contents):
                self.print_stacks(stacks)
                if len(stacks) < i+1:
                    stacks.append([])
                stacks[i] = stacks[i] or []
                if content.isalpha():
                    stacks[i].insert(0, content)
        return stacks

    @staticmethod
    def read_moves(move_lines):
        moves = []
        for line in move_lines:
            md = re.findall(r'move (\d+) from (\d+) to (\d+)', line)
            if md:
                move = {'num': int(md[0][0]),
                        'from': int(md[0][1]),
                        'to': int(md[0][2])}
                moves.append(move)
        return moves


if __name__ == "__main__":
    obj = SupplyStacks(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
