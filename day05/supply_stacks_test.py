import unittest
import tempfile
from supply_stacks import SupplyStacks


class SupplyStacksTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            '    [D]    ',
            '[N] [C]    ',
            '[Z] [M] [P]',
            ' 1   2   3 ',
            '',
            'move 1 from 2 to 1',
            'move 3 from 1 to 3',
            'move 2 from 2 to 1',
            'move 1 from 1 to 2',
        ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return SupplyStacks(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 'CMZ')

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 'MCD')


if __name__ == '__main__':
    unittest.main()
