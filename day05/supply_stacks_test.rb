# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './supply_stacks'

describe SupplyStacks do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { SupplyStacks.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) do
    <<~TXT
          [D]#{'    '}
      [N] [C]#{'    '}
      [Z] [M] [P]
      1   2   3#{' '}

      move 1 from 2 to 1
      move 3 from 1 to 3
      move 2 from 2 to 1
      move 1 from 1 to 2
    TXT
  end

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal('CMZ')
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal('MCD')
    end
  end
end
