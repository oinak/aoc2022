#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'fileutils'

# Day scaffold generator
class InitDay
  attr_reader :day, :title

  def initialize(title:, day: nil)
    @day = (day || Date.today.day).to_i
    @title = title
  end

  def generate
    create_dir
    create_files
  end

  def create_files
    create_file("#{path}/#{underscore(title)}.rb", file_draft)
    create_file("#{path}/#{underscore(title)}_test.rb", test_draft)
    create_file("#{path}/statement.md", 'FILL ME')
    create_file("#{path}/input.txt", 'FILL ME')
    create_file("#{path}/test.txt", 'FILL ME')
  end

  def create_file(filepath, content)
    File.open(filepath, 'w') { |f| f << content }
  end

  def file_draft
    <<~FILE
      # frozen_string_literal: true

      # Advent of code - Day #{day}
      class #{title}
        def initialize(filename)
          data = File.read(filename)
        end

        def solution(advanced: false)
          advanced ? 0 : 1
        end
      end

      if __FILE__ == $PROGRAM_NAME
        obj = #{title}.new(ARGV[0])
        puts obj.solution
        puts obj.solution(advanced: true)
      end
    FILE
  end

  def test_draft
    <<~FILE
      # frozen_string_literal: true

      require 'minitest/autorun'
      require 'minitest/pride'
      require_relative './#{underscore(title)}'

      describe #{title} do
        before do
          @tempfile = Tempfile.new('input.txt')
          @tempfile.write(lines)
          @tempfile.close
        end

        subject { #{title}.new(@tempfile.path) }

        after do
          @tempfile.delete
        end

        let(:lines) do
          <<~TXT
            <COPY TEST DATA HERRE>
          TXT
        end

        describe 'with basic situation' do
          it 'returns the correct answer' do
            _(subject.solution).must_equal('<SOLUTION>')
          end
        end

        describe 'with advanced situation' do
          it 'returns the cost of cheapest aligment' do
            _(subject.solution(advanced: true)).must_equal('<NEW SOLUTION>')
          end
        end
      end
    FILE
  end

  def underscore(str)
    word = str.dup
    word.gsub!('::', '/')
    word.gsub!(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
    word.gsub!(/([a-z\d])([A-Z])/, '\1_\2')
    word.tr!('-', '_')
    word.downcase!
    word
  end

  private

  def create_dir = FileUtils.mkdir_p(path)

  def path = @path ||= format('./day%02d', @day)
end

InitDay.new(title: ARGV[0], day: ARGV[1]).generate if __FILE__ == $PROGRAM_NAME
