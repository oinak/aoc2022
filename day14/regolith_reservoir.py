import sys
from itertools import pairwise


class RegolithReservoir:
    """ Advent of code - Day 14 """

    ROCK = '#'
    AIR = '.'
    SOURCE = '+'
    SAND = 'o'

    def __init__(self, filename):
        with open(filename) as f:
            lines = [line.strip().split(' -> ') for line in f.readlines()]
            # print(f'lines: {lines}')
            self.data = [self.list_of_int_pairs(line) for line in lines]
            self.source = (500, 0)
            self.grid = {self.source: self.SOURCE}
            self.floor = None

    @staticmethod
    def list_of_int_pairs(line):
        """
        receives: [['498,4', '498,6', '496,6']]
        returns:  [[498, 4], [498, 6], [496, 6]]
        """
        # print(f'line: {line}')
        return list(map(lambda x: list(map(int, x.split(','))), line))

    def solution(self, advanced=False):
        return self.count_til_block() if advanced else self.count_sand()

    def count_sand(self):
        self.map_cave()
        self.monitor_sand_fall()
        return list(self.grid.values()).count(self.SAND)

    def map_cave(self):
        for line in self.data:
            for (a, b) in pairwise(line):
                self.fill_tiles(a, b)

    def fill_tiles(self, pt_a, pt_b):
        # print(f'pt_a: {pt_a}, pt_b: {pt_b}')
        ax, ay = pt_a
        bx, by = pt_b
        step_x = (ax < bx) - (ax > bx) or 1
        step_y = (ay < by) - (ay > by) or 1
        for y in range(ay, by + step_y, step_y):
            for x in range(ax, bx + step_x, step_x):
                self.grid[(x, y)] = self.ROCK

    def monitor_sand_fall(self):
        rest = True
        while rest:
            rest = self.sand_fall()
        self.display_grid()

    def sand_fall(self):
        """return True if rest, False if falls forever"""
        position = self.source
        x, y = position
        while y < self.grid_bottom():
            down = (x, y + 1)
            down_left = (x - 1, y + 1)
            down_right = (x + 1, y + 1)

            if self.grid.get(down) is None:
                position = down
            else:  # if self.grid.get(down) in (self.SAND, self.ROCK):
                if self.grid.get(down_left) is None:
                    position = down_left
                elif self.grid.get(down_right) is None:
                    position = down_right
                else:
                    self.grid[position] = self.SAND  # rest
                    return True
            x, y = position
        return False  # false forever

    def grid_bottom(self):
        return max(map(lambda p: p[1], self.grid))

    def display_grid(self, falling=(None, None)):
        # print(f'grid: {self.grid}')
        xs = list(map(lambda point: point[0], self.grid))
        ys = list(map(lambda point: point[1], self.grid))
        # print(f'xs: {xs}, ys: {ys}')
        for y in range(min(ys)-1, max(ys) + 2):
            for x in range(min(xs)-1, max(xs) + 2):
                if (x, y) == falling:
                    print('*', end='')
                elif self.floor and y == self.floor:
                    print(self.ROCK, end='')
                elif (x, y) in self.grid:
                    print(self.grid[(x, y)], end='')
                else:
                    print(self.AIR, end='')
            print()
        print()

    def count_til_block(self):
        self.map_cave()
        self.floor = self.grid_bottom() + 2
        self.monitor_block_fall()
        return list(self.grid.values()).count(self.SAND)

    def monitor_block_fall(self):
        blocked_source = False
        while blocked_source is False:
            blocked_source = self.fall_with_floor()
            # self.display_grid()
        self.display_grid()

    def fall_with_floor(self):
        """return True if source, else False"""
        position = self.source
        x, y = position
        while y < self.floor:
            # self.display_grid((x, y))
            # print(f'y: {y}, floor: {self.floor}, y + 1 == flor:{y + 1 == self.floor}')
            down = (x, y + 1)
            down_left = (x - 1, y + 1)
            down_right = (x + 1, y + 1)

            if (y + 1) == self.floor:
                self.grid[position] = self.SAND  # rest
                return False
            elif self.grid.get(down) is None:
                position = down
            else:  # if self.grid.get(down) in (self.SAND, self.ROCK):
                if self.grid.get(down_left) is None:
                    position = down_left
                elif self.grid.get(down_right) is None:
                    position = down_right
                else:
                    self.grid[position] = self.SAND  # rest
                    return position == self.source
            x, y = position
        raise Exception('should not reach here')


if __name__ == "__main__":
    obj = RegolithReservoir(sys.argv[1])
    print(obj.solution(False))
    obj = RegolithReservoir(sys.argv[1])
    print(obj.solution(advanced=True))
