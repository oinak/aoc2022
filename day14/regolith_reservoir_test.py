import unittest
import tempfile
from regolith_reservoir import RegolithReservoir


class RegolithReservoirTest(unittest.TestCase):
    def lines(self):
        return '\n'.join([
            '498,4 -> 498,6 -> 496,6',
            '503,4 -> 502,4 -> 502,9 -> 494,9'
            ])

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return RegolithReservoir(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 24)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 93)


if __name__ == '__main__':
    unittest.main()
