import unittest
import tempfile
from tuning_trouble import TuningTrouble


class TuningTroubleTest(unittest.TestCase):
    def lines(self):
        return 'mjqjpqmgbljsphdztnvjfqwrcgsmlb'

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile()
        with open(self.tempfile.name, 'w') as tmp:
            tmp.write(self.lines())

    def tearDown(self):
        self.tempfile.close()

    def subject(self):
        return TuningTrouble(self.tempfile.name)

    def test_basic(self):
        self.assertEqual(self.subject().solution(), 7)

    def test_advanced(self):
        self.assertEqual(self.subject().solution(advanced=True), 19)


if __name__ == '__main__':
    unittest.main()
