# frozen_string_literal: true

# Advent of code - Day 6
class TuningTrouble
  attr_reader :data

  def initialize(filename)
    @data = File.read(filename).chomp.chars
  end

  def solution(advanced: false) = start_of_marker(advanced ? 14 : 4)

  def start_of_marker(width)
    data.each_cons(width).with_index do |chars, i|
      return width + i if chars.uniq.length == width
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  obj = TuningTrouble.new(ARGV[0])
  puts obj.solution
  puts obj.solution(advanced: true)
end
