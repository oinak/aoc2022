# frozen_string_literal: true

require 'minitest/autorun'
require 'minitest/pride'
require_relative './tuning_trouble'

describe TuningTrouble do
  before do
    @tempfile = Tempfile.new('input.txt')
    @tempfile.write(lines)
    @tempfile.close
  end

  subject { TuningTrouble.new(@tempfile.path) }

  after do
    @tempfile.delete
  end

  let(:lines) { 'mjqjpqmgbljsphdztnvjfqwrcgsmlb' }

  describe 'with basic situation' do
    it 'returns the correct answer' do
      _(subject.solution).must_equal(7)
    end
  end

  describe 'with advanced situation' do
    it 'returns the cost of cheapest aligment' do
      _(subject.solution(advanced: true)).must_equal(19)
    end
  end
end
