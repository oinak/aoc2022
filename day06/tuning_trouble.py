import sys


class TuningTrouble:
    """ Advent of code - Day 6 """

    def __init__(self, filename):
        with open(filename) as f:
            self.data = f.read()

    def solution(self, advanced=False):
        return self.find_marker(14) if advanced else self.find_marker(4)

    def find_marker(self, size):
        for i, cons in enumerate(self.each_cons(self.data, size)):
            if len(set(cons)) == size:
                return i + size  # index of last char

    @staticmethod
    def each_cons(xs, n):  # inspired by Ruby's Enumerable
        return [xs[i:i+n] for i in range(len(xs)-n+1)]


if __name__ == "__main__":
    obj = TuningTrouble(sys.argv[1])
    print(obj.solution(False))
    print(obj.solution(advanced=True))
